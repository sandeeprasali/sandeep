const form = document.getElementById('form');
const fname = document.getElementById('fname');

    
     
form.addEventListener('submit', (e) => {
    e.preventDefault();

    checkInputs();

});

function checkInputs(){
    // get the values from the Inputs 
    const fnameValue = fname.value.trim();
    
    if(fnameValue === ''){
        //show error 
        // add error class 
        setErrorFor(fname, 'First name cannot be blank'); 
    }else{
        setSuccessFor(fname);
    }
}
    function setErrorFor(input, message){
        const formControl = input.parentElement;
        const small = formControl.querySelector('small');

        small.innerText = message;

        formControl.className = 'form-control error'; 

    }
    function setSuccessFor(input){
        const formControl = input.parentElement; 
        formControl.className = 'form-control success';
    }
    
    
 
    
    
